<!DOCTYPE html>
<html lang="en">
<head>
  <title>Arquivo TXT / CSV</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Formulário</h2>
  <form class="navbar-form navbar-left" role="search" action="lerArquivoTXT.php">
  	<ul>
  		<li>
  			<div class="form-group">
	    		<input class="form-control" name="myFile" type="file">
	  		</div>
	  	</li>
  		<li>
  			<button type="submit" class="btn btn-default">Gerar Arquivo CSV</button>
  		</li>
  	</ul>
  	  
	</form>
</div>

</body>
</html>
