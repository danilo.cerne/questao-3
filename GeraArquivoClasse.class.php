<?php  
class GeraArquivo {
	
	public function lerTxt($filename) {
		// Abre o Arquvio no Modo r (para leitura)
		$arquivo = fopen($filename, 'r');
		
		// Lê o conteúdo do arquivo 
		while(!feof($arquivo))
		{
			//Mostra uma linha do arquivo
			$linha = fgets($arquivo, 1024);
			echo $linha.'<br />';
		}
		
		// Fecha arquivo aberto
		fclose($arquivo);
	}

	public function arrayToCsv($array) {
		header("Content-type: text/csv");
		header("Content-Disposition: attachment; filename=file.csv");
		header("Pragma: no-cache");
		header("Expires: 0");

		outputCSV(array(
		    array("name 1", "age 1", "city 1"),
		    array("name 2", "age 2", "city 2"),
		    array("name 3", "age 3", "city 3")
		));

		function outputCSV($data) {
	    	$output = fopen("php://output", "w");
	    	foreach ($data as $row) {
	        	fputcsv($output, $row); // here you can change delimiter/enclosure
	    	}
	    	fclose($output);
		}

	}
}
?>